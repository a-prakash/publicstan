import re
import sys

sys.path.append("..")


from src.stan import first, get_menu, get_contact, mitsuku, get_date_events
from src.stan import get_event_dates, get_period_events, is_in_DB, get_form, get_details
import pytest
import facebook


def test_first_response():
    source = first("s")["source"]
    assert source == "test json source", "initial test failed"


def test_get_form():
    """tests if events were read from google sheets over a period"""
    response = get_form("maintenance", "1744725888878006")
    result = response["fulfillmentMessages"][0]["payload"]["facebook"]["attachment"][
        "payload"
    ]["text"]
    assert result == "Here is the maintenance form!", "didn't get form"


def test_is_not_DB():
    """tests if events were read from google sheets over a period"""
    response = is_in_DB("1699223936758887")  # my dad who is not in Stan's DB
    assert not response, "My dad should not be in the DB"


def test_get_details():
    """tests if events were read from google sheets over a period"""
    response = get_details("1744725888878006")
    assert response[0] == "Arjun", "first name wrong"
    assert response[1] == "Prakash", "last name wrong"
    assert response[2] == "1744725888878006", "fid wrong"


def test_is_in_DB():
    """tests if events were read from google sheets over a period"""
    response = is_in_DB("1744725888878006")
    assert response, "Arjun was not in the DB for some reason"


def test_get_period_events():
    """tests if events were read from google sheets over a period"""
    response = get_period_events(
        "2019-08-05T12:00:00+10:00", "2019-08-10T12:00:00+10:00"
    )
    source = response["source"]
    speech = response["fulfillmentText"]
    print(speech)
    assert source == "from get_period_events", "didn't execute method"


def test_get_event_dates():
    """tests if all dates for an event are read from google sheets"""
    response = get_event_dates("Victory Dinner")
    source = response["source"]
    speech = response["fulfillmentText"]
    print(speech)
    assert source == "from get_event_dates", "didn't execute method"


def test_get_date_events():
    """tests if events were read from google sheets on a single date"""
    response = get_date_events("2019-08-05T12:00:00+10:00")
    source = response["source"]
    speech = response["fulfillmentText"]
    print(speech)
    assert source == "from get_date_events", "didn't execute method"


def test_get_menu():
    """tests if the menu was read from google sheets"""
    response = get_menu("lunch", "2019-07-01T12:00:00+10:00")
    source = response["source"]
    speech = response["fulfillmentText"]
    assert "Monday lunch" in speech, "didn't read from spreadsheet"
    assert source == "from get_menu", "didn't execute method"


def test_get_contact():
    """tests if the contact directory was read from google sheets"""
    response = get_contact("Principal")
    source = response["source"]
    speech = response["fulfillmentText"]
    assert "Wayne Erickson" in speech, "didn't read from spreadsheet"
    assert source == "from get_contact", "didn't execute method"


'''
Got a cease and desist from mitsuku creator lmao
def test_mitsuku():
    """tests if the contact directory was read from google sheets"""
    response = mitsuku("I like you")
    status = response["status"]
    speech = response["fulfillmentText"]
    source = response["source"]
    assert "ok" in status, "didn't reach mitsuku"
    assert source == "from mitsuku", "didn't execute method"
'''
