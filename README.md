# Stan Droid

This is the [orignal college chatbot](https://www.facebook.com/StanDroidBot) for St Andrew's College.

*__Please note: This is a public version of the repo with all keys and credentials removed - so it will not actually run__*

## Introduction
Stan droid uses a number of different services to work.

Dialogflow: This manages the natural language understanding

Heroku: This hosts the code

CircleCI: Mannages the continuous intergration so that nothing breaks

Bitbucket(git): The version control

Firebase: a no-sql database that contains users

## Code structure
The code that controls Stan Droid is in the ```src``` folder and is called ```stan.py ```

The ```test``` folder contains unit tests which will automatically be called by circleCI. Please add to these tests as you add features. [Read the docs here.](https://docs.pytest.org/en/latest/)

The ```scripts``` folder contains one off programs that check that the spreadsheets are being read. The only important script is ```popDB.py``` as you will need to run this to update the database.

The ```secrets``` folder contains the access keys to my google drive, heroku etc and ```sheets.googleapis.com-python.json``` is they key to my google sheets (I couldn't figure out how to make it work whilst in the secrets folder)

## Instructions

### Setting up


First clone the repo from bitbucket and ```cd``` into this  ``` standroid``` folder. Then set up the environment (you only need to do this the first time).
```
python3 -m venv venv #create a virtual environment
. venv/bin/activate #activate environment
pip3 install --upgrade setuptools #install dependencies
pip3 install --upgrade gcloud
pip install -r requirements.txt
````

### Committing
Now that its set up, you just need to run, after you make a change :

```
git add -A
git commit -m "your commit message"
git push
```
and cicrcleCI will take care of the rest. (you may get a warning from Black which will reformat the code for you. If this happens just run the above code again

### Ask for Help
I've tried to make this as stable as possible so be sure to mess around. If something breaks, circleCI will catch it!

Just make sure you're using version control properly. [This is a great interactive tutorial](https://learngitbranching.js.org/)
