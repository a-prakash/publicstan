"""Script to populate the database
First you need to get the data in json format
then run the script
"""

import pyrebase
import json

toke = (
    toke
) = "redacted"
config = {
    "redacted"
}

firebase = pyrebase.initialize_app(config)

db = firebase.database()


with open("data/data19.json", "r") as f:
    data = json.load(f)


for user in data["users"]:
    db.child("users").child(user["App ID"]).set(user)
    print(user["Name"])
