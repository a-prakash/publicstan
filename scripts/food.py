"""Script that reads the food spreadsheet"""


import pygsheets
import time

import sys

sys.path.append("..")
gc = pygsheets.authorize(credentials_directory="..")

# Open spreadsheet and then workseet
sh = gc.open("food")
wks = sh.sheet1
menu = "Saturday lunch"

start = time.time()
speech = ""
meals = wks.get_all_values()
for meal in meals:
    if meal[0] == menu:
        for cell in meal:
            if cell != "":
                speech += cell


end = time.time()
print(end - start)
print(speech)
