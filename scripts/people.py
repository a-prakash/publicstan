"""Script that reads the people spreadsheet"""

import pygsheets

import time

import sys

sys.path.append("..")
gc = pygsheets.authorize(credentials_directory="..")

# Open spreadsheet and then workseet

# open google sheet
sh = gc.open("Drews Contact")
sheet = sh.sheet1
speech = ""
position = "Principal"

# not elegant but way the fastest way to do it
people = sheet.get_all_values()
for person in people:
    # find row with meal
    if person[0] == position:
        # append all information in row to speech
        for cell in person:
            if cell != "":
                speech += cell + "\n"

print(speech)

# Update a cell with value (just to let him know values is updated ;) )
