#!/usr/bin/env python

import urllib
import json
import os, time
import datetime
import calendar
from datetime import date
import collections
import pygsheets
from flask import Flask
from flask import request
from flask import make_response

import random
import re

import bs4
import requests

# import facebook
import pyrebase
import facebook

from os import sys

# maddies comment

sys.path.append("../secrets")
gc = pygsheets.authorize()
os.environ["TZ"] = "Australia/Melbourne"
time.tzset()

# Pyrebase set up
config = {
    "redacted"
}

firebase = pyrebase.initialize_app(config)
db = firebase.database()


# Flask app should start in global layout
app = Flask(__name__)

DEBUG = False  # turn on to debug through dialogflow console


@app.route("/webhook", methods=["POST"])
def webhook():
    """
    app entry point, recieves post requests

    Returns:
        json: returns response
    """
    req = request.get_json(silent=True, force=True)

    if DEBUG:
        fid = "REDACTED"  # Arjun's fid
    else:
        fid = (
            req.get("originalDetectIntentRequest")
            .get("payload")
            .get("data")
            .get("sender")
            .get("id")
        )

    if is_in_DB(fid):
        res = process_request(req, fid)
    else:
        res = bad_request(req, fid)

    res = json.dumps(res, indent=4)  # turn string nto json

    r = make_response(res)  # turn into response object
    r.headers["Content-Type"] = "application/json"  # tel object it is json

    return r


def process_request(req, fid):
    """
    Helper method to extract action and parameters to be used
    Args:
        req (json): raw json input

    Returns:
        json: appropriate response
    """
    queryResult = req.get("queryResult")
    action = queryResult.get("action")
    parameters = queryResult.get("parameters")

    if action == "test":
        res = first(req)
    elif action == "get_menu":
        meal = parameters.get("meal")
        date = parameters.get("date")
        res = get_menu(meal, date)
    elif action == "get_contact":
        staff = parameters.get("staff")
        res = get_contact(staff)
    elif action == "get_event_dates":
        event = parameters.get("event")
        res = get_event_dates(event)
    elif action == "get_date_events":
        date = parameters.get("date")
        res = get_date_events(date)
    elif action == "get_period_events":
        start_date = parameters.get("date-period").get("startDate")
        end_date = parameters.get("date-period").get("endDate")
        res = get_period_events(start_date, end_date)
    elif action == "get_form":
        form = parameters.get("form")
        res = get_form(form, fid)
    elif action == "input.unknown":
        unknown_text = queryResult["queryText"]
        res = mitsuku(unknown_text)
    return res


def first(req):
    "dummy method to test pytest"
    speech = "first return"
    res = {
        "fulfillmentText": speech,
        "displayText": "this is display",
        # "data": data,
        # "contextOut": [],
        "source": "test json source",
    }
    return res


def bad_request(req, fid):
    """
    Helper method to extract action and parameters to be used
    Invoked if the user is not in the database
    Args:
        req (json): raw json input
        fid (string): facebook id

    Returns:
        json: appropriate response
    """
    queryResult = req.get("queryResult")
    action = queryResult.get("action")
    parameters = queryResult.get("parameters")

    if action == "get_form":
        form = parameters.get("form")
        res = get_form(form, fid)
    elif action == "input.unknown":
        unknown_text = queryResult["queryText"]
        res = mitsuku(unknown_text)
    else:
        res = default_bad_req()
    return res


def default_bad_req():
    """
    Default message if user is not in the DB and wants to use a feature
    Returns:
        json: appropriate response
    """

    speech = "you are not in the database. If you are a member of St Andrew's contact Rhiannon to regain access"

    res = {
        "fulfillmentText": speech,
        "displayText": "this is display",
        # "data": data,
        # "contextOut": [],
        "source": "from default_bad_req",
    }

    return res


def get_details(fid):
    """
    Helper method to extract name from facebook id
    Args:
        fid (string): raw json input

    Returns:
        tuple (string): first_name, last_name, fid
    """

    graph = facebook.GraphAPI(
        access_token="redacted"
    )

    first_name = graph.get_object(fid).get("first_name")
    last_name = graph.get_object(fid).get("last_name")

    return first_name, last_name, str(fid)


def is_in_DB(fid):
    """
    Helper method to check if a user is in the database
    Args:
        fid (string): facebook id

    Returns:
        bool: true if user in DB
    """

    details = get_details(fid)
    firstName = details[0]
    lastName = details[1]
    fid = details[2]

    isAndrovian = False

    match = db.child("users").child(str(fid)).get()
    if not match.val():
        return False
    else:
        return True


def get_form(form, fid):
    """
    returns a link to the requested form
    Args:
        form (string): requested form
        fid (string) : facebook id

    Returns:
        json: appropriate form
    """

    details = get_details(fid)
    firstName = details[0]
    lastName = details[1]
    fid = details[2]

    speech = firstName + " " + lastName + " " + fid

    if form == "sign_up":

        payload = {
            "template_type": "button",
            "text": "Here is the sign up form!",
            "buttons": [
                {
                    "type": "web_url",
                    "url": "redacted"
                    + str(fid)
                    + "&entry.1933753676="
                    + firstName
                    + "+"
                    + lastName,
                    "title": "Go to form!",
                }
            ],
        }
    elif form == "AAT":
        payload = {
            "template_type": "button",
            "text": "Here is the AAT form!",
            "buttons": [
                {
                    "type": "web_url",
                    "url": "redacted",
                    "title": "Go to form!",
                }
            ],
        }
    elif form == "maintenance":
        payload = {
            "template_type": "button",
            "text": "Here is the maintenance form!",
            "buttons": [
                {
                    "type": "web_url",
                    "url": "redacted",
                    "title": "Go to form!",
                }
            ],
        }

    res = {
        "fulfillmentMessages": [
            {
                "payload": {
                    "facebook": {"attachment": {"type": "template", "payload": payload}}
                }
            }
        ]
    }

    return res


def get_menu(meal, date):
    """
    Reads and returns menu from google sheet
    Args:
        meal (string): requested meal
        date (string): datetime as a string

    Returns:
        json: Returns menu
    """

    # defualt to today if no date is given
    if date == "":
        today = datetime.datetime.now()
        date = today.strftime("%Y-%m-%d")
    else:
        date = date.split("T")[0]

    d = datetime.datetime.strptime(date, "%Y-%m-%d").date()
    day = calendar.day_name[d.weekday()]  # convert date to day of the week

    # open google sheet
    sh = gc.open("food")
    wks = sh.sheet1
    menu = day + " " + meal
    speech = ""

    # not elegant but way the fastest way to do it
    meals = wks.get_all_values()
    for meal in meals:
        # find row with meal
        if meal[0] == menu:
            # append all information in row to speech
            for cell in meal:
                if cell != "":
                    speech += cell + "\n"

    res = {
        "fulfillmentText": speech,
        "displayText": "this is display",
        # "data": data,
        # "contextOut": [],
        "source": "from get_menu",
    }
    return res


def get_contact(position):
    """
    Reads and returns contact info from google sheet
    Args:
        position (string): the requested person/staff member

    Returns:
        json: Returns the person's contact information
    """
    # open google sheet
    sh = gc.open("Drews Contact")
    sheet = sh.sheet1
    speech = ""

    # not elegant but way the fastest way to do it
    people = sheet.get_all_values()
    for person in people:
        # find row with meal
        if person[0] == position:
            # append all information in row to speech
            for cell in person:
                if cell != "":
                    speech += cell + "\n"

    res = {
        "fulfillmentText": speech,
        "displayText": "this is display",
        # "data": data,
        # "contextOut": [],
        "source": "from get_contact",
    }
    return res


def get_date_events(date):
    """
    reads calendar and gets the events on the given date
    Args:
        date (string): the requested date

    Returns:
        json: Returns the events on given date
    """

    speech = ""
    rday = date.split("T")[0]

    d = datetime.datetime.strptime(rday, "%Y-%m-%d")
    formatd = d.strftime("%d-%m-%Y")

    sh = gc.open("Calendar")
    wks = sh.sheet1
    cal = wks.get_all_values(include_tailing_empty_rows=False)

    for day in cal:
        list_day = list(filter(None, day))
        print(list_day)
        if str(list_day[0]) == str(formatd) and len(list_day) != 1:
            for event in list_day[1:-1]:
                speech += str(event) + ", "
            speech += list_day[-1]
            speech += "\n\n"

    if speech == "":
        speech = "Doesn't seem to be anything on"

    res = {
        "fulfillmentText": speech,
        "displayText": "this is display",
        # "data": data,
        # "contextOut": [],
        "source": "from get_date_events",
    }
    return res


def get_event_dates(event):
    """
    reads calendar and gets the  dates of given event
    Args:
        date (event): the requested event

    Returns:
        json: Returns the the dates of the event
    """


    sh = gc.open("Calendar")
    wks = sh.sheet1
    cal = wks.get_all_values()

    speech = ""

    today = datetime.datetime.now().date()

    for day in cal:
        day = list(filter(None, day))
        if any(event.lower() in s.lower() for s in day):

            date = datetime.datetime.strptime(day[0], "%d-%m-%Y").date()

            dow = calendar.day_name[date.weekday()]
            matching = str(
                [s for s in day if event.lower() in s.lower()][0]
            )  # real shit form tbh

            if date > today:
                speech += matching + ": " + dow + " " + date.strftime("%d-%b") + "\n\n"

    if speech == "":
        speech = "That event isn't in the calendar :/"

    res = {
        "fulfillmentText": "All results for " + event + "\n" + speech,
        "displayText": "this is display",
        # "data": data,
        # "contextOut": [],
        "source": "from get_event_dates",
    }
    return res


def get_period_events(start_date, end_date):
    """
    reads calendar and gets the events between two dates
    Args:
        start_date (string): the requested date
        end_date (string): the requested date

    Returns:
        json: Returns the events on between the two dates
    """

    speech = ""

    start_date = start_date.split("T")[0]
    end_date = end_date.split("T")[0]

    start = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    sh = gc.open("Calendar")
    wks = sh.sheet1
    cal = wks.get_all_values()

    for day in cal:
        day[0] = datetime.datetime.strptime(day[0], "%d-%m-%Y")
        day = list(filter(None, day))
        if day[0] > start and len(day) != 1:

            dow = calendar.day_name[day[0].weekday()]

            dayf = day[0].strftime("%d-%b")

            speech += dayf + " (" + dow + "): "
            for event in day[1:-1]:
                speech += str(event) + ", "
            speech += day[-1]
            speech += "\n\n"

            # day[0] = datetime.datetime.strptime(day[0], '%d-%m-%Y')
        if day[0] > end:
            break

    if speech == "":
        speech = "There is nothing over that period"

    res = {
        "fulfillmentText": speech,
        "displayText": "this is display",
        # "data": data,
        # "contextOut": [],
        "source": "from get_period_events",
    }
    return res




if __name__ == "__main__":

    port = int(os.getenv("PORT", 5000))

    print("Starting app on port %d" % port)

    app.run(debug=False, port=port, host="0.0.0.0")
